FROM ubuntu
RUN apt update -y 
RUN apt install python3  python3-pip python3-venv python3-dev -y
RUN pip3 install --upgrade pip 
RUN apt-get remove python3-pip -y
ENV DEBIAN_FRONTEND=noninteractive
ENV TZ 'America/Argentina/Buenos_Aires'
RUN echo $TZ > /etc/timezone && \
    apt-get update && apt-get install -y tzdata && \
    rm /etc/localtime && \
    ln -snf /usr/share/zoneinfo/$TZ /etc/localtime && \
    dpkg-reconfigure -f noninteractive tzdata && \
    apt-get clean