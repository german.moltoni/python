#convertir .ui to .py
# #python3.10 -m PyQt5.uic.pyuic form_abm.ui -o form_abm.py -x
from form_abm import Ui_Dialog
from PyQt5 import QtWidgets
from PyQt5.QtWidgets import QApplication, QTableWidgetItem
from datetime import date
from datetime import timedelta
import traceback,sys
from os import path

class Ui_IYM(QtWidgets.QMainWindow, Ui_Dialog):
    def __init__(self,parent=None):
        super(Ui_IYM, self).__init__(parent)
        self.setupUi(self)
        self.set_cmb_moneda()
        self.dt_valor.setDate(date.today())
       # self.tab_tbls.currentChanged.connect(self.cambia_tab)
        #fec_hasta = self.dt_hasta.date()

       # self.btn_buscar.clicked.connect(self.buscar)#agrego evento a boton
#        self.btn_exporta.setDisabled(True) #habilita o no el boton
       # self.set_fcias() carga el combobox
       # self.dt_desde.setDate(date.today() - timedelta(days=1)) #setea date
       # self.dt_hasta.setDate(date.today())
       # self.tab_tbls.setCurrentIndex(0) Selecciona el tab
    #    self.cambia_tab(0)
       # self.tbl_oper.horizontalHeader().setVisible(True)#muestra header caja

    
    def set_cmb_moneda(self):
        lst_monedas = list()
        lst_monedas.append({"id":1,"nombre":"Dolar"})
        lst_monedas.append({"id":2,"nombre":"Euro"})
        for moneda in lst_monedas:
            self.cmb_moneda.addItem(moneda["nombre"], moneda["id"])

if __name__ == '__main__':
    try:    
        app = QApplication(sys.argv)
        form = Ui_IYM()  
        form.show()
        sys.exit(app.exec_())  
    except Exception as err: 
        traceback.print_exc()       
        print(err)  
