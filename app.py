#pip install pymysql 
#pip freeze > requeriments.txt
#pip install -r requeriments.txt
from json import loads
from pymysql import connect
from pymysql.cursors import DictCursor

if __name__ == "__main__":
    credentials = dict()
    my_credentials = dict()
    with open("./config.json","r") as config_file:
        dct_config = loads(config_file.read())
        if "credentials" in dct_config.keys():
            credentials = dct_config["credentials"]
            if "mysql" in credentials.keys():
                my_credentials = credentials["mysql"]

    if len(my_credentials) > 0:
        my_conn = connect(host=my_credentials["host"],user=my_credentials["user"],password=my_credentials["password"],database="test",port=3306,charset="utf8mb4",cursorclass=DictCursor)
        with my_conn.cursor() as cursor:
            str_query = "select * from Ftable LIMIT 100"
            cursor.execute(str_query)
            lst_result = cursor.fetchall()
            print("Query 1 : "+str_query)
            if len(lst_result) > 0:
                for res in lst_result:
                    print(res)

            str_query = "select * from Ftable where fecfac=%s"
            cursor.execute(str_query,["2022-05-01"])
            lst_result = cursor.fetchall()

            print("Query 2 : "+str_query)
            if len(lst_result) > 0:
                for res in lst_result:
                    print(res)

            