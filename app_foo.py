#pip install pymysql 
#pip freeze > requeriments.txt
#pip install -r requeriments.txt
from ast import match_case
from json import loads
from nis import match
from unittest import case
from pymysql import connect
from pymysql.cursors import DictCursor




def conexion(my_credentials,db):
    try:
        my_conn = connect(host=my_credentials["host"],user=my_credentials["user"],password=my_credentials["password"],database=db,port=3306,charset="utf8mb4",cursorclass=DictCursor)
    except Exception as err:
        return {"msg":err,"res":False}
    return my_conn

def query_exec(conn,query,data=None):
    try:
        with conn.cursor() as cursor:
            if data is not None:
                cursor.execute(query,data)
            else:
                cursor.execute(query)
            conn.commit()
            lst_result = cursor.fetchall()
            return lst_result
    except Exception as err:
        return {"msg":err,"res":False}
        

def muestra_menu():
    print("1.Crear Tipo de entidad\n")
    print("2.Consultar tipo de entidades\n")
    print("3.Modificar tipo de entidad\n")
    print("4.Baja tipo de entidad\n")
    opt = input("Ingresar opcion: ")
    return opt

if __name__ == "__main__":
    credentials = dict()
    my_credentials = dict()
    with open("./config.json","r") as config_file:
        dct_config = loads(config_file.read())
        if "credentials" in dct_config.keys():
            credentials = dct_config["credentials"]
            if "mysql" in credentials.keys():
                my_credentials = credentials["mysql"]

    if len(my_credentials) > 0:
        conn = conexion(my_credentials,"EJEMPLO")
        if  not isinstance(conn,dict):
            opt = muestra_menu()
            if opt == "1":
                nombre = input("Nombre: ").strip().capitalize()
                descripcion = input("Descripcion: ").strip().capitalize()
                if len(nombre) > 0 and len(descripcion) > 0:
                    insert_query = "INSERT INTO tipo_entidad (nombre,descripcion,estado) VALUES ('{}','{}',{})".format(nombre,descripcion,int(True))
                    res = query_exec(conn,insert_query)
                    if not isinstance(res,dict):
                        print("Se realizo el alta de forma correcta")
                    else:
                        print(res["msg"])
            if opt == "2":
                select_query = "SELECT * from tipo_entidad"
                res = query_exec(conn,select_query)
                if not isinstance(res,dict):
                    if len(res) > 0:
                        for tent in res:
                            print("ID:{}  Nombre:{}  Descripcion:{}".format(tent["id"],tent["nombre"].capitalize(),tent["descripcion"].capitalize()))

            if opt == "3":
                nombre = input("Ingresar nombre de entidad: ").strip().capitalize()
                select_query = "SELECT * from tipo_entidad where nombre like '%{}%' and estado=1".format(nombre)
                res = query_exec(conn,select_query)
                if not isinstance(res,dict):
                    if len(res) > 0:
                        for tent in res:
                            print("ID:{}  Nombre:{}  Descripcion:{}".format(tent["id"],tent["nombre"].capitalize(),tent["descripcion"].capitalize()))
                        nombre = input("Nombre: ").strip().capitalize()
                        descripcion = input("Descripcion: ").strip().capitalize()
                        if nombre != res[0]["nombre"] and descripcion != res[0]["descripcion"]:
                            update_query = "UPDATE tipo_entidad SET nombre='{}' , descripcion='{}' where id={}".format(nombre,descripcion,res[0]["id"])
                        if nombre == res[0]["nombre"] and descripcion != res[0]["descripcion"]:
                            update_query = "UPDATE tipo_entidad SET   descripcion='{}' where id={}".format(descripcion,res[0]["id"])
                        if nombre != res[0]["nombre"] and descripcion == res[0]["descripcion"]:
                            update_query = "UPDATE tipo_entidad SET nombre='{}'  where id={}".format(nombre,res[0]["id"])
                        res = query_exec(conn,update_query)
                        if not isinstance(res,dict):
                            print("Se realizo la modificacion de forma correcta")
                        else:
                            print(res["msg"])
            if opt == "4":
                nombre = input("Ingresar nombre de entidad: ").strip().capitalize()
                select_query = "SELECT * from tipo_entidad where nombre like '%{}%'".format(nombre)
                res = query_exec(conn,select_query)
                if not isinstance(res,dict):
                    if len(res) > 0:
                        for tent in res:
                            print("ID:{}  Nombre:{}  Descripcion:{}".format(tent["id"],tent["nombre"].capitalize(),tent["descripcion"].capitalize()))
                        opt = input("Desea dar la baja? s/n: ").strip().lower()
                        if "s" == opt:
                            update_query = "UPDATE tipo_entidad SET estado='{}'  where id={}".format(int(False),res[0]["id"])
                            res = query_exec(conn,update_query)
                            if not isinstance(res,dict):
                                print("Se realizo la baja de forma correcta")
                            else:
                                print(res["msg"])
                        elif "n" == opt:
                            print("No se realizo la baja")
                        else:
                            print("Opcion incorrecta")
